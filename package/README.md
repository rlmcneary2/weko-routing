# @rlmcneary2/weko-routing

Here at **weko** we're bringing the fun back to creating for the web!

All our tools:

- Run directly in the web browser! No preprocessor or bundling needed.
- Use standard web technologies like module imports and web components.

We believe in the power of the web⚡and are making it as easy to create powerful web pages today as
it was back in 1993. Open a text editor and start building!

Other weko packages that make web components better:

- **[weko-lifecycle](https://codeberg.org/rlmcneary2/weko-lifecycle)** — listen to a single event for
  lifecycle updates.
- **[weko-render](https://codeberg.org/rlmcneary2/weko-render)** — simplify custom element DOM manipulation and
  performance.

See the example app (TODO) to learn how to utilize web browsers' modern features while building
awesome web applications that don't require extensive tech stacks just for bundling.

**Contents**

- [Usage](#usage)
- [API](#api)

## Usage

This package supports routing for web components with the following features:

- dynamic segments
- module loading based on a segment

This package does not add or remove elements from the DOM, it's up to the web component to take
action based on the results of routing information that's provided by the `weko-changed-event`
event. For example if a `childSegment` property is provided it's up to the web component to decide
if it should add children to, or remove children from, the DOM - or do nothing.

👉 _The weko-routing package includes TypeScript type support._

### Add routing functionality

`WekoRoutingMixin` extends classes implementing a web component; the classes must be, or extend,
`HTMLElement`. This mixin adds the routing capabilities to the class constructor that's returned.

#### Custom element

To get started, extend a custom element class by invoking `WekoRoutingMixin` and passing it the base
type which must be `HTMLElement` or a class that extends `HTMLElement`.

If you're using standard [web component lifecycle
methods](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements#lifecycle_callbacks)
you will need to override the function and invoke the base class methods through the `super`
keyword. In the example below the static `observedAttributes` and method `attributeChangedCallback`
are overridden, the overrides both invoke `super`.

👉 _All the examples are written in TypeScript, but JavaScript is also supported._

```ts
import { WekoRoutingMixin } from "https://esm.sh/@rlmcneary2/weko-routing";

class PeopleComponent extends WekoRoutingMixin(HTMLElement) {
  constructor() {
    super();
  }

  static override get observedAttributes(): string[] {
    return [...(super.observedAttributes as string[]), ...OTHER_ATTRIBUTE_ARRAY];
  }

  override attributeChangedCallback(name: string, oldValue: string | null, newValue: string): void {
    super.attributeChangedCallback && super.attributeChangedCallback(name, oldValue, newValue);
  }

  // class implementation
}

if (!window.customElements.get("wc-people")) {
  window.customElements.define("wc-people", PeopleComponent);
}
```

#### Weko attributes

Element attributes are used to configure web components and their place in a route. The route is
controlled by the construction of the DOM tree (i.e. nesting of elements).

There are two ways to setup routing attributes for your web components: as static HTML attributes on
your web component at build time, or applied to a JavaScript web component using API functionality
at run time.

##### **Static HTML attributes**

We can create static HTML with routing attributes.

```html
<app-container weko-segment="/">
  <wc-people weko-segment="people"></wc-people>
  <wc-about weko-segment="about"></wc-about>
</app-container>
```

In the example above several [<span style="white-space:nowrap">`weko-segment`</span>
attributes](https://rlmcneary2.codeberg.page/weko-routing/docs/variables/WEKO_ATTRIBUTE_SEGMENT.html)
have been set. A "segment" is a piece of a route or pathname. For example if the pathname is
`/people` then "people" is a segment. Maybe you noticed in the example that `app-container` has the
segment "/". This segment - "/" - is the segment that refers to the root of the path and "/" is only
used to define the web component that corresponds to the root of the pathname. A component with its
`weko-segment` set to "/" is **required**.

Components can also have a dynamic segment — the pathname segment value will vary — by using the
[<span style="white-space:nowrap">`weko-param`</span>
attribute](https://rlmcneary2.codeberg.page/weko-routing/docs/variables/WEKO_ATTRIBUTE_PARAM.html).
This will make the component's segment available from the class's
[`params`](https://rlmcneary2.codeberg.page/weko-routing/docs/interfaces/WekoRoutingType.html#params)
property.

```html
<app-container weko-segment="/">
  <wc-people weko-segment="people">
    <wc-person weko-param=":personId"></wc-person>
  </wc-people>
</app-container>
```

In the previous example the path "/people/123" would result in `params` containing an entry
"personId: 123".

##### **Runtime attributes**

The routing attributes can be set when a web component is created programmatically. You can use the
constants `WEKO_ATTRIBUTE_SEGMENT` and `WEKO_ATTRIBUTE_PARAM` for the attribute names.

```ts
const people = document.createElement("wc-people");
people.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "people");
this.append(people);

const person = document.createElement("wc-person");
people.setAttribute(WEKO_ATTRIBUTE_PARAM, ":personId");
people.append(person);
```

### Responding to browser URL changes

You can react to a change in the browser's URL by listening for [<span
style="white-space:nowrap">`weko-changed-event`</span>](https://rlmcneary2.codeberg.page/weko-routing/docs/variables/WEKO_CHANGED_EVENT.html).
Typically the data provided by this event would be used to determine which elements should be part
of the DOM and which elements should not. You may be interested in the [WekoRender
package](https://codeberg.org/rlmcneary2/weko-render) which helps with DOM manipulation.

```ts
import {
  WEKO_CHANGED_EVENT,
  isWekoChangedEventPathname,
  WekoRoutingMixin
} from "https://esm.sh/@rlmcneary2/weko-routing";

class PeopleComponent extends WekoRoutingMixin(HTMLElement) {
  constructor() {
    super();

    this.addEventListener(WEKO_CHANGED_EVENT, this.#handleWekoChangedEvent);
  }

  #handleWekoChangedEvent(evt: Event): void {
    if (!isWekoChangedEventPathname(evt)) {
      return;
    }

    const { pathname } = evt.detail.pathname;
    // Use the `pathname` in some way.
  }
}
```

### Changing the browser pathname

The mixin makes it possible for a component to change the browser URL's pathname via its `pushState`
method.

```ts
import {
  WEKO_CHANGED_EVENT,
  isWekoChangedEventPathname,
  WekoRoutingMixin
} from "https://esm.sh/@rlmcneary2/weko-routing";

class PeopleComponent extends WekoRoutingMixin(HTMLElement) {
  #pathname: string | undefined;

  constructor() {
    super();

    this.addEventListener(WEKO_CHANGED_EVENT, this.#handleWekoChangedEvent);
  }

  connectedCallback(): void {
    super.connectedCallback && super.connectedCallback();

    const button = document.createElement("button");
    button.textContent = "Nav to Person";
    button.addEventListener("click", () => {
      this.#pathname && this.pushState(joinPath(this.#pathname, "123"));
    });

    this.append(button);
  }

  #handleWekoChangedEvent(evt: Event): void {
    if (!isWekoChangedEventPathname(evt)) {
      return;
    }

    const { match, pathname } = evt.detail.pathname;
    this.#pathname = match ? pathname : undefined;
  }
}
```

Let's break down the previous example. First, the component subscribes to the `weko-changed-event`.

```ts
  constructor() {
    super();

    this.addEventListener(WEKO_CHANGED_EVENT, this.#handleWekoChangedEvent);
  }
```

When an event is received the `pathname` value is stored in the private data member `#pathname`.

```ts
  #handleWekoChangedEvent(evt: Event): void {
    if (!isWekoChangedEventPathname(evt)) {
      return;
    }

    const { match, pathname } = evt.detail.pathname;

    if (match) {
      this.#pathname = pathname;
    }
  }
```

When the component is connected to the document it makes a button available. Finally, when the
button is clicked, the browser's URL is altered by invoking the `pushState` method.

```ts
  connectedCallback(): void {
    super.connectedCallback && super.connectedCallback();

    const button = document.createElement("button");
    button.textContent = "Nav to Person";
    button.addEventListener("click", () => {
      this.#pathname && this.pushState(joinPath(this.#pathname, "123"));
    });

    this.append(button);
  }
```

### Loading modules for a pathname

When the pathname changes you may want to load ES modules that are associated with a child segment
(if it exists). This process is simplified by overriding the `segmentToModulePathname` method.
Return an empty array in cases where no modules should be loaded. The modules will be loaded
asynchronously.

```ts
  override segmentToModulePathname(segment: string | undefined): string[] {
    segment === "groups" ? return ["/component/groups.js"] : [];
  }
```

## API

There is detailed information about the mixin components and interfaces in the [API
Documentation](https://rlmcneary2.codeberg.page/weko-routing/docs/)

## Installation

### As part of an ES module

Use "esm.sh" and import this module into a source file where you need to use
routing.

```ts
import from "https://esm.sh/@rlmcneary2/weko-routing";
```

### As part of a bundle

If you are bundling your source code you may need to use a dynamic `import` to
load the library like so:

```ts
async function main() {
  await import("https://esm.sh/@rlmcneary2/weko-routing");
}
```

### Using a script element

The script can also be loaded in an HTML file, typically as part of the `<head>`
element.

```html
<head>
  <script src="https://esm.sh/@rlmcneary2/weko-routing" type="module"></script>
</head>
```

### TypeScript support

To use Typescript during development you need to do three things.

1. Install the package locally from npm under devDependencies: <span style="white-space:nowrap">`npm
i -D @rlmcneary2/weko-routing`</span>.
2. Update the tsconfig.json file's `compilerOptions.paths` with the local
   installation of the package.

```json
"paths": {
  "@rlmcneary2/weko-routing": ["./node_modules/@rlmcneary2/weko-routing/src/index"]
}
```

3. Create a `.d.ts` file somewhere in a path processed by tsconfig.json that
   declares a module for the remote address.

```ts
// remote.d.ts

// Allow all module names starting with "https://". This will suppress TS errors.
declare module "https://*";

// Map the URL to all the exports from the locally installed package.
declare module "https://esm.sh/@rlmcneary2/weko-routing" {
  export * from "@rlmcneary2/weko-routing";
}
```
