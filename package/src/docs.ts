/**
 * This files exists only to generate TypeDoc documentation. It is not part of the build or
 * deployed.
 */
export * from "./index.ts";
import type { Constructor } from "./WekoRouting.ts";

export type { Constructor };
