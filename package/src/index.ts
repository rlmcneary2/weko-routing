import type { WekoChangedEventPathnameDetails, WekoRoutingType } from "./types.ts";
import {
  WEKO_ATTRIBUTE_PARAM,
  WEKO_ATTRIBUTE_SEGMENT,
  isWekoChangedEventPathname
} from "./shared.ts";
import { joinPath } from "./pathname.ts";
import { WekoRoutingMixin } from "./WekoRouting.ts";

export type { WekoChangedEventPathnameDetails, WekoRoutingType };
export {
  WEKO_ATTRIBUTE_PARAM,
  WEKO_ATTRIBUTE_SEGMENT,
  isWekoChangedEventPathname,
  joinPath,
  WekoRoutingMixin
};
export * from "./shared.ts";
