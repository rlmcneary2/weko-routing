import type { WekoChangedEventPathnameDetails, WekoRoutingType } from "./types.ts";

/**
 * The name of the {@link !CustomEvent} emitted when something interesting has happened.
 *
 * @remarks
 * See {@link WekoChangedEventPathnameDetails} for information about this event's details.
 */
export const WEKO_CHANGED_EVENT = "weko-changed-event";

/**
 * Name of the attribute that defines that this element's route segment is a param.
 *
 * @remarks
 * When set on an element, and when this element appears in the current route, the corresponding
 * value in the pathname will become a value in {@link WekoRoutingType["params"]}.
 *
 * The value of the attribute **must** start with a colon (:), e.g. `:groupId`.
 *
 * The attribute value will have the colon prefix removed to create the name of the param.
 *
 * @example
 * ```html
 * <wc-app weko-segment="/">
 *   <wc-groups weko-segment="groups">
 *     <wc-group weko-param=":groupId">
 *     </wc-group>
 *   </wc-groups>
 * </wc-app>
 * <!-- pathname = "/group/s123" -->
 * <!-- route    = "/groups/:groupId" -->
 * <!-- params   = { groupId: "123" } -->
 * ```
 */
export const WEKO_ATTRIBUTE_PARAM = "weko-param";

/**
 * Name of the attribute that defines this element's route segment.
 *
 * @example
 * ```html
 * <wc-app weko-segment="/">
 *   <wc-groups weko-segment="groups">
 *   </wc-groups>
 * </wc-app>
 * ```
 */
export const WEKO_ATTRIBUTE_SEGMENT = "weko-segment";

export function isWekoRoutingElement(obj: any): obj is WekoRoutingType {
  return obj && "wekoRoute" in obj;
}

/**
 * A [type
 * predicate](https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates)
 * to narrow an `obj` (usually an {@link !Event} instance) to a {@link !CustomEvent} with a
 * {@link WekoChangedEventPathnameDetails} detail.
 * @param obj The object to be tested
 * @returns True if `obj` has a `detail.type` that equals "pathname."
 */
export function isWekoChangedEventPathname(
  obj: any
): obj is CustomEvent<WekoChangedEventPathnameDetails> {
  return obj && "detail" in obj && "type" in obj.detail && obj.detail.type === "pathname";
}
