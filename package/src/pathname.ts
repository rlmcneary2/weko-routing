import type { WekoChangedEventPathnameDetails } from "./types.ts";
import { WEKO_ATTRIBUTE_PARAM, WEKO_ATTRIBUTE_SEGMENT, isWekoRoutingElement } from "./shared.ts";

export function getPathnameData(
  element: HTMLElement,
  pathname: string
): WekoChangedEventPathnameDetails["pathname"] {
  if (!pathname) {
    return { match: false, pathname };
  }

  // `weko-pathname` example values
  // "/"
  // "/characters"
  // "/characters/99"

  const elementWekoPathname = isWekoRoutingElement(element)
    ? element.wekoRoute ?? undefined
    : undefined;

  const elementSegment = element.getAttribute(WEKO_ATTRIBUTE_SEGMENT);
  const elementParam = element.getAttribute(WEKO_ATTRIBUTE_PARAM);

  if (!elementWekoPathname && !elementSegment && !elementParam) {
    return { match: false, pathname };
  }

  if (!elementWekoPathname || !elementWekoPathname.length) {
    return { match: false, pathname };
  }

  const pathnameParts = createPathnameParts(pathname);
  const elementPathnameParts = createPathnameParts(elementWekoPathname || "");
  if (pathnameParts.length < elementPathnameParts.length) {
    return { match: false, pathname };
  }

  let childSegment: string | undefined;
  let resultParams: Record<string, string> | undefined;
  const left: string[] = [];
  for (let i = 0; i < pathnameParts.length; i++) {
    const pathnameSegment = pathnameParts[i];

    if (i < elementPathnameParts.length) {
      const elementPathnameSegment = elementPathnameParts[i];

      if (elementPathnameSegment.startsWith(":")) {
        resultParams = resultParams || {};
        resultParams[elementPathnameSegment.substring(1)] = pathnameSegment;
      } else if (pathnameSegment !== elementPathnameSegment) {
        // This component apparently shouldn't be in the DOM...
        return { match: false, pathname };
      }
    }

    // If we're at the end of the current component's path then the next item in the pathname (if
    // there is one) is this component's child segment or a param.
    if (i + 1 === elementPathnameParts.length) {
      if (!elementParam) {
        childSegment = i + 1 < pathnameParts.length ? pathnameParts[i + 1] : undefined;
      }
    }
  }

  const result: ReturnType<typeof getPathnameData> = {
    match: true,
    pathname
  };

  if (childSegment) {
    result["childSegment"] = childSegment;
  }

  if (resultParams) {
    result["params"] = resultParams;
  }

  return result;
}

/**
 * Given a set of strings this will return them joined together as a rooted path. It does not
 * have a trailing "/".
 * @param pathname A pathname string, a string that may contain "/" characters.
 * @param segments Individual path segments, may NOT contain "/" characters.
 * @returns An absolute path constructed from the arguments.
 */
export function joinPath(pathname: string, ...segments: string[]): string {
  return `/${[...pathname.split("/"), ...segments]
    .filter(item => !!item && item !== "/")
    .join("/")}`;
}

function createPathnameParts(pathname: string): string[] {
  const pathnameParts = trimPathnameSolidus(pathname).split("/");
  !pathname || pathname === "/" ? (pathnameParts[0] = "/") : pathnameParts.unshift("/");
  return pathnameParts;
}

function trimPathnameLeadingSolidus(pathname: string): string {
  let output = pathname;
  while (0 < output.length && output.startsWith("/")) {
    output = output.substring(1);
  }

  return output;
}

function trimPathnameSolidus(pathname: string): string {
  let output = trimPathnameLeadingSolidus(pathname);

  while (0 < output.length && output.endsWith("/")) {
    output = output.slice(0, -1);
  }

  return output;
}
