import { describe, expect, it } from "vitest";
import "@testing-library/jest-dom/vitest";
import { WEKO_ATTRIBUTE_PARAM, WEKO_ATTRIBUTE_SEGMENT } from "./shared";
import { getPathnameData } from "./pathname";

describe(`${WEKO_ATTRIBUTE_SEGMENT} attribute`, () => {
  it("matches '/' element segment to '/' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");
    (elem as any).wekoRoute = "/";

    const result = getPathnameData(elem, "/");
    expect(result).toEqual({ match: true, pathname: "/" });
  });

  it("matches '/' element segment to '/one' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");
    (elem as any).wekoRoute = "/";

    const result = getPathnameData(elem, "/one");
    expect(result).toEqual({ childSegment: "one", match: true, pathname: "/one" });
  });

  it("matches '/' element segment to '/one/two' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");
    (elem as any).wekoRoute = "/";

    const result = getPathnameData(elem, "/one/two");
    expect(result).toEqual({ childSegment: "one", match: true, pathname: "/one/two" });
  });

  it("matches 'one' element segment to '/one' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "one");
    (elem as any).wekoRoute = "/one";

    const result = getPathnameData(elem, "/one");
    expect(result).toEqual({ match: true, pathname: "/one" });
  });

  it("matches 'one' element segment to '/one/two' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "one");
    (elem as any).wekoRoute = "/one";

    const result = getPathnameData(elem, "/one/two");
    expect(result).toEqual({ childSegment: "two", match: true, pathname: "/one/two" });
  });

  it("matches 'two' element segment to '/one/two' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "two");
    (elem as any).wekoRoute = "/one/two";

    const result = getPathnameData(elem, "/one/two");
    expect(result).toEqual({ match: true, pathname: "/one/two" });
  });

  it("does NOT match '/one' element pathname to '/a' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "one");
    (elem as any).wekoRoute = "/one";

    const result = getPathnameData(elem, "/a");
    // console.log("--- result=", JSON.stringify(result));
    expect(result).toEqual({ match: false, pathname: "/a" });
  });

  it("does NOT match '/one/two' element pathname to '/one' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "two");
    (elem as any).wekoRoute = "/one/two";

    const result = getPathnameData(elem, "/one");
    expect(result).toEqual({ match: false, pathname: "/one" });
  });

  it("does NOT match undefined element pathname", () => {
    const elem = document.createElement("div");

    const result = getPathnameData(elem, "/");
    expect(result).toEqual({ match: false, pathname: "/" });
  });

  it("does NOT match '/' element pathname to '' pathname", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");

    const result = getPathnameData(elem, "");
    expect(result).toEqual({ match: false, pathname: "" });
  });
});

describe(`${WEKO_ATTRIBUTE_PARAM} attribute`, () => {
  it("matches the param value to its id", () => {
    const elem = document.createElement("div");
    elem.setAttribute(WEKO_ATTRIBUTE_PARAM, ":id");
    (elem as any).wekoRoute = "/:id";

    const result = getPathnameData(elem, "/123");
    expect(result).toEqual({ match: true, params: { id: "123" }, pathname: "/123" });
  });

  it("matches multiple param values ids", () => {
    const person = document.createElement("div");
    person.setAttribute(WEKO_ATTRIBUTE_PARAM, ":personId");
    (person as any).wekoRoute = "/groups/123/people/:personId";

    const group = document.createElement("div");
    group.setAttribute(WEKO_ATTRIBUTE_PARAM, ":groupId");
    (group as any).wekoRoute = "/groups/:groupId";

    let result = getPathnameData(group, "/groups/123");
    expect(result).toEqual({ match: true, params: { groupId: "123" }, pathname: "/groups/123" });

    result = getPathnameData(person, "/groups/123/people/abc");
    expect(result).toEqual({
      match: true,
      params: { personId: "abc" },
      pathname: "/groups/123/people/abc"
    });
  });
});
