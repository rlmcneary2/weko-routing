/**
 * This detail object is emitted in a {@link WEKO_CHANGED_EVENT} {@link !CustomEvent} when the
 * browser URL's pathname changes.
 *
 * @remarks
 * This detail is included in the `weko-changed-event` ({@link WEKO_CHANGED_EVENT}). Emitted
 *  when the browser's pathname changes.
 *
 * - Add an event listener to an element produced by {@link WekoRoutingMixin} to be informed when
 *   the URL's pathname changes.
 * - Typically this event is used to determine when to update the children of this object based on
 *   the `childSegment`.
 *
 * @example Subscribe to pathname changes
 * ```ts
 * this.addEventListener(WEKO_CHANGED_EVENT, evt => {
 *   console.log(evt.detail);
 *   // "{ ...WekoChangedEventPathnameDetails }"
 * });
 * ```
 */
export interface WekoChangedEventPathnameDetails {
  /** Information related to a "pathname" change event. */
  pathname: {
    /**
     * The pathname segment that follows this web component's segment.
     *
     * @remarks
     * Given the pathname `/group/person` <br/>and this element's {@link WEKO_ATTRIBUTE_SEGMENT}
     * attribute value is "group" <br/>then the `childSegment` is "person"
     */
    childSegment?: string;

    /** True if the current pathname matches this element's {@link WEKO_ATTRIBUTE_SEGMENT}. */
    match: boolean;

    /** {@inheritDoc WekoRoutingType#params} */
    params?: Record<string, string>;

    /** The browser's current pathname. */
    pathname: string;
  };
  /** The type of the WekoChangedEventPathnameDetails. */
  type: "pathname";
}

/**
 * The {@link WekoRoutingMixin} returns a class constructor that implements this interface.
 *
 * @remarks
 * The methods and properties of this interface allow a web component to manipulate the browser's
 * URL and use the URL to drive the appearance of the UI.
 *
 * The construction of classes with this interface is why the <span style="white-space:
 * nowrap;">weko-routing</span> package exists.
 */
export interface WekoRoutingType extends HTMLElement {
  /**
   * Name and value of params extracted from the path.
   *
   * @remarks
   * Params are set using a {@link WEKO_ATTRIBUTE_PARAM} attribute on an element. <br/>Given a route
   * `/groups/:groupId/people/:personId` <br/>and a path `/group/123/people/abc` <br/>params will
   * have the value `{ groupId: "123", personId: "abc" }`
   */
  readonly params: Record<string, string> | undefined;

  /**
   * Returns the full route to this element.
   *
   * @remarks
   * The parent node's `wekoRoute` with this element's `weko-segment` or `weko-param` appended. For
   * example: <span style="white-space: nowrap">`/people/:personId`</span>
   *
   * Note that the route is **not** a path, it contains params (prefixed with ':').
   */
  readonly wekoRoute: string;

  /**
   * Update the browser's path.
   *
   * @remarks
   * Will trigger the {@link WEKO_CHANGED_EVENT} event. Elements without
   *  {@link WEKO_ATTRIBUTE_PARAM} and {@link WEKO_ATTRIBUTE_SEGMENT} can also invoke this method
   *  and cause a browser change.
   *
   * @param pathname - The browser path to set - absolute.
   */
  pushState(pathname: string): void;

  /**
   * Override to load one or more ESM modules associated with a segment.
   *
   * @remarks
   * Override this method and return an array of paths to modules. Invoked whenever there is a
   * change the component's status such as when it is connected or the attributes change. This
   * probably what you want to use to load the module associated with children of this element.
   *
   * @example
   * ```ts
   * override segmentToModulePathname(segment: string): string[] {
   *   if(segment === "groups"){
   *     return ["/components/group.js"];
   *   }
   *   return [];
   * }
   * ```
   *
   * @param segment - The weko-segment value
   * @returns An array of pathnames to ESM modules that can be dynamically imported. Return an empty
   * array when no modules should be loaded.
   * @virtual
   */
  segmentToModulePathname(segment: string | undefined): string[];

  /**
   * Sets the routing information provided in the options onto the children of the parent.
   *
   * @remarks
   * This is a utility function to set attributes on elements that are created programmatically at
   * runtime.
   *
   * @param parent - An object with a collection of child elements.
   * @param parent.children - These children will have their routing parameters set.
   * @param options - Data for this function.
   * @param options.childSegment - This value will be set as each child's `weko-segment` attribute.
   * @param options.filter - If provided, each child in the collection will be passed to this
   * function; return true if the child should have attributes applied. If not provided every child
   * will have attributes applied.
   */
  setChildRoutingAttributes(
    parent: {
      children: HTMLCollection | Element[];
    },
    options: {
      childSegment: string;
      filter?: (/** The child to examine. */ child: Element) => boolean;
    }
  ): void;
}
