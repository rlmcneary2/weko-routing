import { WEKO_ATTRIBUTE_SEGMENT } from "./shared.ts";

/**
 * Sets the routing information of the parent on its children; including: weko-pathname.
 * @param parent This parent's children will have their routing parameters set by using the values
 * of this instance.
 * @param childSegment
 * @param childPathname Usually provided by the return value of caller's
 * `this._getChildPathname()`.
 * @param elementNamePrefixes If provided the names of the child elements will be tested to see if
 * they start with this prefix.
 */
export function setChildRoutingAttributes(
  parent: { children: HTMLCollection | Element[] },
  {
    childSegment,
    filter
  }: {
    childSegment: string;
    /** Return true if the child should have attributes applied. If not provided the child will have
     * attributes applied.*/
    filter?: (child: Element) => boolean;
  }
): void {
  for (const child of parent.children) {
    if (child.nodeType !== 1) {
      continue;
    }

    if (filter && !filter(child)) {
      continue;
    }

    child.setAttribute(WEKO_ATTRIBUTE_SEGMENT, childSegment);
  }
}
