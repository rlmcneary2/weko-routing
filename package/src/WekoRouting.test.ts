import { beforeEach, describe, expect, it, vi } from "vitest";
import "@testing-library/jest-dom/vitest";
import { type WekoRoutingType } from "./types";
import { WekoRoutingMixin } from "./WekoRouting";
import { WEKO_ATTRIBUTE_PARAM, WEKO_ATTRIBUTE_SEGMENT, WEKO_CHANGED_EVENT } from "./shared";

beforeEach(() => {
  // Some of the tests depend on window event listeners so we need to clean up the elements we
  // attached to the body element.
  Array.from(document.getElementsByTagName("body")[0].children).forEach(child => child.remove());
});

describe("WekoRouting.pathnameChanged", () => {
  it("`pathnameChanged` is invoked when the element is connected; segment match", () => {
    window.history.pushState(null, "", "/");

    const elem = document.createElement("test-component");
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");

    document.body.append(elem);
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: { pathname: { match: true, pathname: "/" }, type: "pathname" }
      })
    );
  });

  it("`pathnameChanged` is invoked when the element is connected; segment doesn't match", () => {
    window.history.pushState(null, "", "/one");
    expect(window.location.pathname).toEqual("/one");

    const elem = document.createElement("test-component");
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "two");

    document.body.append(elem);
    // console.log(handler.mock.lastCall[0].detail);
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: { pathname: { match: false, pathname: "/one" }, type: "pathname" }
      })
    );
  });
});

describe("WekoRouting.params", () => {
  it("creates a collection of params", () => {
    window.history.pushState(null, "", "/groups/123/people/abc");

    const person = document.createElement("test-component");
    const personHandler = vi.fn();
    person.addEventListener(WEKO_CHANGED_EVENT, personHandler);
    person.setAttribute(WEKO_ATTRIBUTE_PARAM, ":personId");

    const people = document.createElement("test-component");
    people.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "people");
    people.append(person);

    const group = document.createElement("test-component");
    const groupHandler = vi.fn();
    group.addEventListener(WEKO_CHANGED_EVENT, groupHandler);
    group.setAttribute(WEKO_ATTRIBUTE_PARAM, ":groupId");
    group.append(people);

    const groups = document.createElement("test-component");
    groups.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "groups");
    groups.append(group);

    const elem = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");
    elem.append(groups);

    document.body.append(elem);
    expect(elem.params).toEqual({ groupId: "123", personId: "abc" });
    expect(groupHandler).toHaveBeenCalledTimes(1);
    expect(groupHandler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          pathname: {
            match: true,
            pathname: "/groups/123/people/abc",
            params: { groupId: "123" }
          },
          type: "pathname"
        }
      })
    );
    expect(personHandler).toHaveBeenCalledTimes(1);
    expect(personHandler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          pathname: {
            match: true,
            pathname: "/groups/123/people/abc",
            params: { groupId: "123", personId: "abc" }
          },
          type: "pathname"
        }
      })
    );
  });
});

describe("WekoRouting.pushState", () => {
  it("sends the custom event", () => {
    window.history.pushState(null, "", "/");

    const elem = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "/");

    document.body.append(elem);

    // Events may be fired when `elem` is appended so clear out those calls.
    handler.mockClear();

    elem.pushState("/one");
    expect(window.location.pathname).toEqual("/one");
    expect(handler).toHaveBeenCalledOnce();
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { childSegment: "one", match: true, pathname: "/one" }
        }
      })
    );

    elem.pushState("/two");
    expect(window.location.pathname).toEqual("/two");
    expect(handler).toHaveBeenCalledTimes(2);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { childSegment: "two", match: true, pathname: "/two" }
        }
      })
    );

    elem.pushState("/one");
  });

  it("allows any element to pushState", () => {
    window.history.pushState(null, "", "/");

    // This element must have no weko attributes.
    const elem = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);

    document.body.append(elem);

    // Events may be fired when `elem` is appended so clear out those calls.
    handler.mockClear();

    elem.pushState("/a");

    expect(window.location.pathname).toEqual("/a");
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          pathname: { match: false, pathname: "/a" },
          type: "pathname"
        }
      })
    );
  });
});

describe("WekoRouting.setChildRoutingAttributes", () => {
  it("set attributes before the child is appended", () => {
    window.history.pushState(null, "", "/one");

    const child = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    child.addEventListener(WEKO_CHANGED_EVENT, handler);

    const parent = document.createElement("test-component") as WekoRoutingType;
    parent.setChildRoutingAttributes({ children: [child] }, { childSegment: "one" });
    expect(child).toHaveAttribute(WEKO_ATTRIBUTE_SEGMENT, "one");
    expect(handler).toHaveBeenCalledTimes(0);

    document.body.append(parent);
    expect(handler).toHaveBeenCalledTimes(0);

    parent.append(child);
    expect(handler).toHaveBeenCalledOnce();
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { match: true, pathname: "/one" }
        }
      })
    );
  });

  it("set attributes after the child is appended", () => {
    window.history.pushState(null, "", "/two");

    const child = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    child.addEventListener(WEKO_CHANGED_EVENT, handler);

    const parent = document.createElement("test-component") as WekoRoutingType;
    document.body.append(parent);
    parent.append(child);

    // No attributes have been set yet so no events should have been raised.
    expect(handler).toHaveBeenCalledTimes(0);

    parent.setChildRoutingAttributes(parent, { childSegment: "two" });
    expect(child).toHaveAttribute(WEKO_ATTRIBUTE_SEGMENT, "two");
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { match: true, pathname: "/two" }
        }
      })
    );
  });
});

describe("WekoRouting.#handlePopstate", () => {
  it("history state is set to the pathname", async () => {
    window.history.pushState(null, "", "/");

    const elem = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);
    elem.setAttribute(WEKO_ATTRIBUTE_SEGMENT, "one");

    document.body.append(elem);
    // console.log("--- ", handler.mock.lastCall[0].detail);
    expect(handler).toHaveBeenCalledOnce();
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { match: false, pathname: "/" }
        }
      })
    );

    handler.mockClear();

    // Current history
    // | active | pathname | state |
    // | --> | "/" | null |
    elem.pushState("/one");
    // Current history
    // | active | pathname | state |
    // |     | "/" | null |
    // | --> | "/one" | "/one" |

    expect(handler).toHaveBeenCalledOnce();

    elem.pushState("/one/two");
    // Current history
    // | active | pathname | state |
    // |     | "/" | null |
    // |     | "/one" | "/one" |
    // | --> | "/one/two" | "/one/two" |

    expect(handler).toHaveBeenCalledTimes(2);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { childSegment: "two", match: true, pathname: "/one/two" }
        }
      })
    );

    // Simplify expecting the mock.
    handler.mockClear();

    window.history.back();
    // Current history
    // | active | pathname | state |
    // |     | "/" | null |
    // | --> | "/one" | "/one" |
    // |     | "/one/two" | "/one/two" |

    await vi.waitFor(() => expect(handler).toHaveBeenCalled());

    // handler.mock.calls.forEach((args, i) => console.log(`--- ${i}=`, args[0].detail));

    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { match: true, pathname: "/one" }
        }
      })
    );
  });

  it("elem with no attributes gets popstate event", async () => {
    window.history.pushState(null, "", "/");

    // `elem` must not have any weko attributes set.
    const elem = document.createElement("test-component") as WekoRoutingType;
    const handler = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, handler);

    document.body.append(elem);
    // console.log("--- ", handler.mock.lastCall[0].detail);

    elem.pushState("/one");
    elem.pushState("/one/two");

    expect(window.location.pathname).toEqual("/one/two");
    expect(handler).toHaveBeenCalledTimes(2);

    // Simplify expecting the mock.
    handler.mockClear();

    window.history.back();
    await vi.waitFor(() => expect(handler).toHaveBeenCalled());

    expect(window.location.pathname).toEqual("/one");
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          type: "pathname",
          pathname: { match: false, pathname: "/one" }
        }
      })
    );
  });
});

class TestComponent extends WekoRoutingMixin(HTMLElement) {
  constructor() {
    super();
  }
}

customElements.define("test-component", TestComponent);
