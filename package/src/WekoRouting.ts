import type { WekoChangedEventPathnameDetails, WekoRoutingType } from "./types.ts";
import {
  WEKO_ATTRIBUTE_PARAM,
  WEKO_ATTRIBUTE_SEGMENT,
  WEKO_CHANGED_EVENT,
  isWekoRoutingElement
} from "./shared.ts";
import { getPathnameData, joinPath } from "./pathname.ts";
import { setChildRoutingAttributes } from "./routing.ts";

const ATTRIBUTES = [WEKO_ATTRIBUTE_PARAM, WEKO_ATTRIBUTE_SEGMENT];

/**
 * Static data containing pathname params. It's not a static member of WekoRouting so that it can be
 * accessed by functions that are external to the class.
 */
let staticParams: Record<string, string> = {};
/**
 * Static data that has the last pathname passed to `pushState`. Prevents pushing a duplicate
 * pathname value.
 */
let lastPushStatePathname = "";

/**
 * An interface implemented by classes that can be used as the base class for the
 * {@link WekoRoutingMixin}.
 *
 * @privateRemarks
 * The Constructor type definition must be in this file for typedoc to generate the right generic
 * information.
 */
export interface Constructor<T = HTMLElement> {
  new (...input: any[]): T;
}

/**
 * A factory function that generates a class constructor for a web component derived from
 * `HTMLElement` that includes {@link WekoRoutingType} functionality.
 *
 * @returns A class constructor that includes the `baseType` functionality and implements
 * {@link WekoRoutingType}.
 *
 * Class instances emit the `weko-changed-event` ({@link WEKO_CHANGED_EVENT}), a CustomEvent with
 * detail information in the shape of {@link WekoChangedEventPathnameDetails}.
 *
 * @remarks
 * Use this mixin when you want to include weko routing capabilities in a web component. It may be
 * combined with other mixins.
 *
 * @example class extends WekoRoutingMixin
 * ```ts
 * class MyWebComponent extends WekoRoutingMixin(HTMLElement) {}
 * ```
 *
 * @example extending with multiple weko mixins
 * ```ts
 * class MyWebComponent extends WekoRoutingMixin(WekoLifecycleMixin(HTMLElement)) {}
 * ```
 *
 * @param baseType - A class constructor for a type that implements the {@link !HTMLElement}
 * interface.
 * @template T - A type of object that implements the {@link !HTMLElement} interface.
 */
export function WekoRoutingMixin<T extends Constructor>(
  baseType: T
): Constructor<WekoRoutingType> & T {
  class WekoRouting extends baseType {
    #connected = false;
    #handlePopstateBound: ((evt: PopStateEvent) => void) | undefined;
    #handlePushStateBound: ((evt: Event) => void) | undefined;
    #lastPathnameEventDetail: WekoChangedEventPathnameDetails["pathname"] | undefined;
    #wekoRoute: string | undefined;

    /**
     * Constructor for the class produced by the Mixin function.
     * @param args
     */
    constructor(...args: any[]) {
      super(...args);
    }

    /**********************************************************************************************
     * API methods
     */
    // #region

    get params(): Record<string, string> {
      return { ...staticParams };
    }

    get wekoRoute(): string {
      if (!this.#connected) {
        return "";
      }

      if (!this.#wekoRoute) {
        let parent = this.parentNode;
        let parentWekoPathname: string | undefined;
        while (parent) {
          if (isWekoRoutingElement(this.parentElement)) {
            parentWekoPathname = this.parentElement.wekoRoute;
            break;
          }

          parent = parent.parentNode;
        }

        const seg =
          this.getAttribute(WEKO_ATTRIBUTE_PARAM) ??
          this.getAttribute(WEKO_ATTRIBUTE_SEGMENT) ??
          "";

        this.#wekoRoute = joinPath(parentWekoPathname || "/", seg);
      }

      // console.log(`[${this.localName}]wekoRoute: #wekoRoute='${this.#wekoRoute}'.`);
      return this.#wekoRoute;
    }

    pushState(pathname: string): void {
      if (lastPushStatePathname === pathname) {
        return;
      }

      lastPushStatePathname = pathname;

      window.history.pushState(pathname, "", pathname);

      window.dispatchEvent(new CustomEvent("pushstate", { detail: { pathname } }));
    }

    segmentToModulePathname(segment: string | undefined): string[] {
      // Override and implement.
      return [];
    }

    setChildRoutingAttributes(
      parent: { children: HTMLCollection | Element[] },
      options: {
        childSegment: string;
        /** Return true if the child should have attributes applied. If not provided the child will
         * have attributes applied.*/
        filter?: (child: Element) => boolean;
      }
    ): void {
      setChildRoutingAttributes(parent, {
        ...options
      });
    }

    // #endregion

    /**********************************************************************************************
     * Web component lifecycle callbacks
     */
    // #region

    /**
     * Web component static lifecycle property `observedAttributes`.
     */
    static get observedAttributes(): string[] {
      // @ts-ignore ...annoying
      return [...(super.observedAttributes ?? []), ...ATTRIBUTES];
    }

    attributeChangedCallback(name: string, oldValue: unknown, newValue: unknown): void {
      this.#wekoRoute = undefined;
      staticParams = {};

      // @ts-ignore ...annoying
      super.attributeChangedCallback && super.attributeChangedCallback(name, oldValue, newValue);

      if (!ATTRIBUTES.includes(name)) {
        return;
      }

      // Attributes can change before the element is mounted or after it's mounted, however if the
      // element isn't connected then the path information will be incorrect because it can't query
      // a parent node, so dispatch the `pathname` event any time attributes are changed after being
      // connected. Only dispatch when our attributes change.
      if (this.#connected) {
        const data = this.#getRouteData(window.location.pathname);
        data && this.#dispatchChangedEvent(data);
      }
    }

    connectedCallback(): void {
      this.#connected = true;
      this.#wekoRoute = undefined;
      staticParams = {};

      // @ts-ignore ...annoying
      super.connectedCallback && super.connectedCallback();

      this.#handlePushStateBound = this.#handlePushStateBound ?? this.#handlePushState.bind(this);
      window.addEventListener("pushstate", this.#handlePushStateBound);

      this.#handlePopstateBound = this.#handlePopstateBound ?? this.#handlePopstate.bind(this);
      window.addEventListener("popstate", this.#handlePopstateBound);

      // When connected invoke pathnameChanged to initialize all the routing code.
      const data = this.#getRouteData(window.location.pathname);
      data && this.#dispatchChangedEvent(data);
    }

    disconnectedCallback(): void {
      this.#connected = false;
      // @ts-ignore ...annoying
      super.disconnectedCallback && super.disconnectedCallback();

      this.#handlePushStateBound &&
        this.removeEventListener("pushstate", this.#handlePushStateBound);
      this.#handlePopstateBound = undefined;

      this.#handlePopstateBound &&
        this.removeEventListener("popstate", this.#handlePopstateBound as EventListener);
      this.#handlePopstateBound = undefined;
    }

    // #endregion

    /**********************************************************************************************
     * private methods
     */
    // #region

    #matchesLastEventDetails(
      detail: WekoChangedEventPathnameDetails["pathname"] | undefined
    ): boolean {
      if (this.#lastPathnameEventDetail === undefined && detail === undefined) {
        return true;
      }

      if (this.#lastPathnameEventDetail !== undefined && detail !== undefined) {
        const { match, pathname, childSegment } = detail;

        if (this.#lastPathnameEventDetail) {
          if (
            this.#lastPathnameEventDetail.childSegment === childSegment &&
            this.#lastPathnameEventDetail.match === match &&
            this.#lastPathnameEventDetail.pathname === pathname
          ) {
            return true;
          }
        }
      }

      return false;
    }

    /**
     * Dispatch a WekoChangedEventDetails event.
     * @param type
     * @param data Any related data will be placed into the event as the value of a key that has the
     * same name as type; e.g. `{detail:{type:"pathname",pathname: data}}`
     */
    #dispatchChangedEvent(
      pathname: WekoChangedEventPathnameDetails["pathname"],
      dispatcher: { dispatchEvent: HTMLElement["dispatchEvent"] } = this
    ): void {
      if (dispatcher === this) {
        if (this.#matchesLastEventDetails(pathname)) {
          return;
        }

        this.#lastPathnameEventDetail = { ...pathname };
      }

      const eventInitDict: CustomEventInit<WekoChangedEventPathnameDetails> = {
        detail: {
          pathname,
          type: "pathname"
        }
      };

      dispatcher.dispatchEvent(new CustomEvent(WEKO_CHANGED_EVENT, eventInitDict));
    }

    #getRouteData(pathname: string): WekoChangedEventPathnameDetails["pathname"] | undefined {
      if (!this.hasAttribute(WEKO_ATTRIBUTE_PARAM) && !this.hasAttribute(WEKO_ATTRIBUTE_SEGMENT)) {
        return;
      }

      const data = getPathnameData(this, pathname);

      // console.log(
      //   `[${
      //     this.localName
      //   }]WekoRouting.#getRouteData: pathname='${pathname}', segment='${this.getAttribute(
      //     WEKO_ATTRIBUTE_SEGMENT
      //   )}', data=`,
      //   data
      // );

      if (data.params) {
        staticParams = { ...staticParams, ...data.params };
      }

      this.#loadRouteModules(data);

      return data;
    }

    #handlePopstate(evt: PopStateEvent): void {
      lastPushStatePathname = "";

      const data = this.#getRouteData(evt.state);
      this.#dispatchChangedEvent(data ?? { match: false, pathname: evt.state });
    }

    #handlePushState(evt: Event): void {
      if (evt && "detail" in evt && "pathname" in (evt as any).detail) {
        const { pathname } = evt.detail as any;

        const data = this.#getRouteData(pathname);
        this.#dispatchChangedEvent(data ?? { match: false, pathname });
      }
    }

    /**
     * Load any modules associated with this route.
     * @param data Information about the current route.
     */
    async #loadRouteModules(data: WekoChangedEventPathnameDetails["pathname"]): Promise<void> {
      const modulePathnames = this.segmentToModulePathname(data.childSegment);
      if (!modulePathnames.length) {
        return;
      }

      const promises = modulePathnames.map(modulePathname => {
        if (!modulePathname) {
          // console.warn(
          //   `[${this.localName}]WekoRouting.#loadRouteData: modulePathname has no value, ignoring.`
          // );
          return Promise.resolve();
        }

        const url = this.baseURI + modulePathname;
        // console.log(`[${this.localName}]WekoRouting.#loadRouteData: loading url='${url}'`);
        return import(url).then(() => {
          // console.log(`[${this.localName}]WekoRouting.#loadRouteData: loaded url='${url}'`);
        });
      });

      await Promise.all(promises);
    }

    // #endregion
  }

  return WekoRouting;
}
